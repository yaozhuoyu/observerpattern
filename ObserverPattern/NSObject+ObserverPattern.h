// See COPYING for licence details.

#import <Foundation/Foundation.h>

typedef void(^GJLObserverBlock)(id observedObject, NSString *key, id oldValue, id newValue);

@interface NSObject (GJLObserverPattern)

- (id)addObserverForKey:(NSString *)key withBlock:(GJLObserverBlock)block;
- (void)removeObserverWithToken:(id)token;

@end
